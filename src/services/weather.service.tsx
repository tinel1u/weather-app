import axios from "axios";
import { ResponseType } from "../types/api";

const API = "c4f8b47c921da72244d4cfa1e7d61eeb";
const instanceAxios = axios.create({
  baseURL: `https://api.openweathermap.org/data/2.5/forecast?appid=${API}&lang=fr&units=metric&`,
});

export const weatherApi = {
  getWeather(name: string) {
    return instanceAxios
      .get<ResponseType>(`&q=${name}`)
      .then((res) => res.data);
  },
};
