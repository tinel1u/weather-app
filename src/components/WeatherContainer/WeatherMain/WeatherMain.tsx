import { FC } from "react";

import { IResponseCity } from "../../../types/api";
import { convertedLocalDate } from "../../../utils/convertedLocalDate";
import { ResultDaysType } from "../../../utils/formattedDate";
import { weatherAverage } from "../../../utils/weatherAverage";

interface IPropsType {
  params: IResponseCity;
  currentWeather: ResultDaysType;
}

const WeatherMain: FC<IPropsType> = ({ params, currentWeather }) => {
  const date = convertedLocalDate(currentWeather.weather[0].dt_txt);
  const population = params.population.toLocaleString();
  const description = currentWeather.weather[0].weather[0].description;
  const pop = (currentWeather.weather[0].pop * 100).toFixed(0);
  const humidity = currentWeather.weather[0].main.humidity.toFixed(0);
  const windSpeed = currentWeather.weather[0].wind.speed.toFixed(2);
  const temp = weatherAverage(currentWeather.weather).toFixed(1);

  return (
    <div className="weather-main">
      <div className="weather__data">
        <div className="weather__more">
          <div className="weather__icon"></div>
          <div className="weather__temp">
            <span>Moyenne quotidienne</span>
            <div>
              {temp} <span>°C</span>
            </div>
          </div>
          <ul className="weather__list">
            <li className="weather__item">
              Risque de précipitations:
              <span> {pop}%</span>
            </li>
            <li className="weather__item">
              Humidité:
              <span> {humidity}%</span>
            </li>
            <li className="weather__item">
              Vent:
              <span> {windSpeed}</span> m/s
            </li>
          </ul>
        </div>
        <div className="weather__info">
          <div className="weather__city">
            {params.name}
            {params.country && ","} <span>{params.country}</span>
          </div>
          <div>Population: {population}</div>
          <div>{date}</div>
          <div>{description}</div>
        </div>
      </div>
    </div>
  );
};

export default WeatherMain;
